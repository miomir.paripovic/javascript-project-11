# Project no. 11

## Task

This is my assignment for week no. 11 and my first JavaScript script. The main goal of the project is to convert an array of strings to array of numbers, sort the array (ascending) and return second smallest and second largest number. The output goes to console (for now).