/* 
 * Zadatak:
 * Napisati JavaScript funkciju koja prima niz numeričkih brojeva i vraća drugi najmanji i drugi najveći broj iz niza.Koristiti sort, push i join * metodu za izradu zadatka
 * Parametar: [13, 105, 75, 1, 344, 6, 5]
 * Rezultat: 5, 105
 */

function convertStringToNumberArray(anArray) {
    let numberArray = [];
    for (let i = 0; i < anArray.length; i++) {
        numberArray.push(Number(anArray[i]));
    }
    //console.log(intArray);
    return numberArray;
}

function cleanDuplicateNumbersFromArray(anArray) {
    let noDuplicates = [];
    noDuplicates.push(anArray[0]);
    for (let i = 0; i < anArray.length; i++) {
        if (anArray[i] !== anArray[i+1] && i+1<anArray.length) {
            noDuplicates.push(anArray[i+1]);
        }        
    }    
    return noDuplicates;
}

function sortANumberArray(anArray) {
    // create a copy of anArray
    let sortedArray = anArray.slice();
    return sortedArray.sort(function(a, b) {return a - b;})    
}

function finalResult(anArray) {
    if (anArray[0] === anArray[1]) {
        return "The second smallest and second largest numbers are the same: ".concat(String(anArray[0]));
    } else {
        return "The second smallest number is: ".concat(String(anArray[0]), "\nThe second largest number is: ", String(anArray[1]), "\n");
    }
}

function fromArrayExtractSecondMinAndMax(anArray) {
    let result2ndMinAndMax = [];
    //console.log(anArray[1]);
    //console.log(anArray[anArray.length-2]);
    result2ndMinAndMax.push(anArray[1], anArray[anArray.length-2]);
    return result2ndMinAndMax;
}

function convertNumberToStringArray(anArray) {
    return anArray.join(", ");
}

// enter an array or use default and print the information
function mainProgram() {
    let stringArray = [];
    while (true) {
        let enterNumber = prompt("Enter a number: ");
        //alert (enterNumber);
        //console.log(typeof(enterNumber));
        if (enterNumber == "default" || enterNumber == "def") {
            stringArray = ["13", "105", "75", "1", "344", "6", "5"];
            break;
        // parseFloat - didn't want 'space' and 'return' values converted to numbers
        } else if (isNaN(parseFloat(enterNumber))) {
            //console.log(enterNumber);
            break;                      
        } else {
            stringArray.push(enterNumber);
        }        
    }
    
    let stringToNumberArray = convertStringToNumberArray(stringArray);    
    let sortedNumberArray = sortANumberArray(stringToNumberArray);
    let noDuplicatesArray = cleanDuplicateNumbersFromArray(sortedNumberArray);
    if (noDuplicatesArray.length < 3) {
        console.log("There are at least 3 different numbers needed to be able to complete this task.");
        return false;
    }

    // this part prints information to console
    console.log("-".repeat(48));
    console.log("Input array (array of strings):\n", stringArray);
    //console.log('test', stringToNumberArray);
    console.log("Input array (array of numbers):\n", stringToNumberArray);
    console.log("Input array sorted (asc):\n", sortedNumberArray);
    console.log("Input array (array of numbers) with no duplicates:\n", noDuplicatesArray);
    console.log("Result array - second min and second max values:\n", fromArrayExtractSecondMinAndMax(noDuplicatesArray));
    console.log("Result array represented as a string:\n", convertNumberToStringArray(fromArrayExtractSecondMinAndMax(noDuplicatesArray)));
    console.log(finalResult(fromArrayExtractSecondMinAndMax(noDuplicatesArray)));
    console.log("-".repeat(48));
    return true;
}